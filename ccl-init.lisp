;;(format t (user-homedir-pathname)) load error
(format t "Clozure CL init file dir is ~A~%" (user-homedir-pathname))
(princ "Clozure CL init file dir is ")
(prin1 (user-homedir-pathname))
;;; The following lines added by ql:add-to-init-file:
#-quicklisp
(let ((quicklisp-init (merge-pathnames "quicklisp/setup.lisp" (user-homedir-pathname))))  
  (when (probe-file quicklisp-init)
    (load quicklisp-init)))
	
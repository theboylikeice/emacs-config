﻿@echo off
set EMACS_HOME=c:/common-tools/emacs-23.4
rem set HOME=c:/common-tools/emacs-23.4
set HOME=D:/GitRepository/emacs-config-git
set SBCL_HOME=c:/common-tools/lisp/sbcl
set CCL_DEFAULT_DIRECTORY=c:/common-tools/lisp/ccl
rem ccl默认采用USERPROFILE做为用户HOME目录,在此设置环境变量改变CCL的HOME目录,以便和sbcl一起使用quicklisp.
set USERPROFILE=%HOME%
set EMACS_SERVER_FILE=%HOME%/.emacs.d/server/server
set PATH=%PATH%;%EMACS_HOME%/bin;%SBCL_HOME%;%CCL_DEFAULT_DIRECTORY%;%HOME%

rem runemacs --debug-init
RunHiddenConsole.exe "c:/common-tools/emacs-23.4/bin/emacsclientw.exe" --alternate-editor="c:/common-tools/emacs-23.4/bin/runemacs.exe" "%1"
 
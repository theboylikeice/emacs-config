@echo off

rem del /S *.elc
rem %EMACS% -batch -q --no-site-file -l cedet-build.el -f cedet-build
rem %EMACS% -batch -q --no-site-file -l -f batch-byte-compile *.el

set EMACS=d:/common-tools/emacs-24.2/bin/emacs.exe
set HOME=d:/common-tools/emacs-24.2
%EMACS% -batch --script init.el -f batch-byte-compile lisps/yasnippet/*.el
rem %EMACS% -batch --script compile1.el -f byte-force-recompile 

REM End of make.bat

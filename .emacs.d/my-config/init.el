;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message (concat "Emacs版本：" emacs-version))
(message (concat "Emacs目录：" (getenv "HOME")))
(message "环境变量:")
(message (getenv "PATH"))
;(setenv "USERPROFILE" "c\:/common-tools/emacs-23\.4")

(defconst my-emacs-path           "~/.emacs.d/my-config/" "我的emacs相关配置文件的路径")
(defconst my-emacs-config-path  (concat my-emacs-path "my-lisps/") "我自己写的emacs lisp包的路径")
(defconst my-emacs-lisps-path     (concat my-emacs-path "lisps/") "我下载的emacs lisp包的路径")
(defconst my-emacs-templates-path (concat my-emacs-path "templates/") "Path for templates")

;; 把`my-emacs-lisps-path'的所有子目录都加到`load-path'里面
(load (concat my-emacs-config-path "my-subdirs")) ;;加载自己写的my-subdirs.el文件
(my-add-subdirs-to-load-path my-emacs-lisps-path) ;;加载下载的el包路径
(my-add-subdirs-to-load-path my-emacs-config-path);;加载自己写的el包路径
 				
(setq byte-compile-warnings nil)

;; 利用`eval-after-load'加快启动速度的库
;; 用eval-after-load避免不必要的elisp包的加载
;; http://emacser.com/eval-after-load.htm
(require 'eval-after-load)

;;常用的变量及函数
(require 'util)
;;一些基本小函数
(require 'misc)

;; 编码设置
(require 'coding-settings)

;;加载自定义配置
(require 'base-config)
;;加载字体设置
(require 'font-config)

;; color theme Emacs主题
(require 'color-theme-settings)
;; 自定义颜色主题
(require 'face-settings)
;;使用color-theme-library插件
;(require 'color-theme-config)

;;加载常用配置
(require 'common-config)

;; 13642605866

;;加载common lisp配置
(require 'common-lisp-config)

;;加载cedet配置
;(require 'cedet-config)
;(require 'cedet-settings)

;; ecb 代码浏览器
;(require 'ecb-settings)

;;像Eclipse那样高亮光标处单词
;(require 'highlight-symbol-settings)

;;自动完成配置
(require 'all-auto-complete-settings)

;;加载java配置
;(require 'java-config)

;;加载自动插入注释插件
;(require 'doxymacs-settings)

;;加载python配置
;(require 'python-config)

;;加载wiki-muse配置
;(require 'wiki-muse-config)
;(require 'wiki-org-config)

;;加载php配置
;(require 'php-config)

;;加载c/c++配置
;(require 'c-config) 
 

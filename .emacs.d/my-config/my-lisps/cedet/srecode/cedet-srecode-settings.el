(message "����srecode")

;; Enable SRecode (Template management) minor-mode.
(global-srecode-minor-mode 1)

(defun cedet-srecode-settings ()
  "Settings for `srecode'.")

(eval-after-load "srecode"
  `(cedet-srecode-settings))

(provide 'cedet-srecode-settings)

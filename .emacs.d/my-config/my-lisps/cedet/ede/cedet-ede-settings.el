(message "����ede")

;; Enable EDE (Project Management) features
(global-ede-mode 1)

(defun cedet-ede-settings ()
  "Settings for `ede'.")

(eval-after-load "ede"
  `(cedet-ede-settings))

(provide 'cedet-ede-settings)

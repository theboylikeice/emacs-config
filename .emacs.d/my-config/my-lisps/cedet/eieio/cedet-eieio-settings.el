(message "����eieio")

(defun cedet-eieio-settings ()
  "Settings for `eieio'.")

(defun chart-settings ()
  "Settings for `chart'."
  (eal-define-keys
   'chart-map
   `(("q"   delete-current-window)
     ("'"   switch-to-other-buffer)
     ("u"   View-scroll-half-page-backward)
     ("SPC" scroll-up)
     ("."   find-symbol-at-point)
     ("1"   delete-other-windows))))

(eval-after-load "chart"
  `(chart-settings))

(eval-after-load "eieio"
  `(cedet-eieio-settings))

(provide 'cedet-eieio-settings)

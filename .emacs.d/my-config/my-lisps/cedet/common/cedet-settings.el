;; -*- Emacs-Lisp -*-
;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "准备加载cedet配置(cedet-settings.el)")

(require 'util)

(when mswin
  (defvar cedet-path (concat my-emacs-lisps-path "cedet") "Path of `cedet'")
  (my-add-subdirs-to-load-path cedet-path))

(require 'cedet)
(require 'cedet-eieio-settings);;EIEIO是一个底层库，它为elisp加入了OO支持。cedet的其它组件都依赖于EIEIO
(require 'cedet-ede-settings);;用来管理项目，它可以把emacs模拟得像一个IDE那样，把一堆文件作为一个project来管理
(require 'cedet-cogre-settings);;全称叫”Connected Graph Editor”，主要和图形相关，比如可以用它来为C++类生成UML图
(require 'cedet-semantic-settings);;Semantic应该是cedet里用得最多的组件了，代码间跳转和自动补全这两大功能都是通过semantic来实现的。
(require 'cedet-srecode-settings);;SRecode是一个模板系统，通过一些预定义的模板，可以很快地插入一段代码
(require 'cedet-speedbar-settings);;Speedbar可以单独创建一个frame，用于显示目录树，函数列表等等

(custom-set-variables 
 '(global-semantic-decoration-mode t nil (semantic-decorate-mode))
 '(global-semantic-highlight-edits-mode t nil (semantic-util-modes))
 '(global-semantic-highlight-func-mode t nil (semantic-util-modes))
 '(global-semantic-idle-scheduler-mode t nil (semantic-idle))
 '(global-semantic-mru-bookmark-mode t nil (semantic-util-modes))
 '(global-semantic-show-parser-state-mode t nil (semantic-util-modes))
 '(global-semantic-show-unmatched-syntax-mode t nil (semantic-util-modes))
 '(global-semantic-stickyfunc-mode nil nil (semantic-util-modes))
 '(global-senator-minor-mode t nil (senator)) 
 '(semanticdb-global-mode t nil (semanticdb))
 '(which-function-mode t)) 

;; 用pulse实现Emacs的淡入淡出效果
;; http://emacser.com/pulse.htm
(require 'pulse-settings)

;;;###autoload
(defun cedet-settings-4-info ()
  "`cedet' settings for `info'."
  (info-initialize)
  (dolist (package `("cogre" "common" "ede" "eieio" "semantic/doc" "speedbar" "srecode"))
    (add-to-list 'Info-directory-list (concat my-emacs-lisps-path "cedet/" package "/"))))

(eval-after-load "info"
  `(cedet-settings-4-info))

(message  "cedet加载完成！")
(provide 'cedet-settings)

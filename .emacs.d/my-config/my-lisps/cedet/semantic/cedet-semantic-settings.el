(message "加载semantic")

;(semantic-load-enable-minimum-features) ;;;;包含以下三个mode
;;;;semantic-idle-scheduler-mode  让cedet在emacs空闲的时候自动分析buffer内容.
;;;;semanticdb-minor-mode 是semantic用来保存分析后的内容的，所以也是应该enable的。
;;;;semanticdb-load-ebrowse-caches  大概的意思好像是semantic可以利用ebrowse的结果。这个feature大概就是把ebrowse生成的文件load给semantic使用。

;(semantic-load-enable-code-helpers)  ;;;;除enable semantic-load-enable-minimum-features之外还包含以下三个mode
;;;;imenu  这个feature可以让imenu显示semantic分析出的类，函数等tags。
;;;;semantic-idle-summary-mode  光标停留在一个类/函数等tag上时，会在minibuffer显示出这个函数原型
;;;;senator-minor-mode 会在emacs上增加一个senator的菜单，可以通过菜单在当前文件的各个tag之间前后移动，跳转；还可以在里面方便地打开/关闭某个feature；
 
;(semantic-load-enable-guady-code-helpers) ;;;;除enable semantic-load-enable-code-helpers之外还包含以下三个mode
;;;; semantic-stickyfunc-mode 这个mode会根据光标位置把当前函数名显示在buffer顶上
;;;; semantic-decoration-mode  semantic会在类/函数等tag上方加一条蓝色的线，源文件很大的时候用它可以提示出哪些是类和函数的头
;;;;semantic-idle-completions-mode  这个mode打开后，光标在某处停留一段时间后，semantic会自动提示此处可以补全的内容

;(semantic-load-enable-excessive-code-helpers)   ;;;;除enable semantic-load-enable-guady-code-helpers之外还包含以下四个mode
;;;; semantic-highlight-func-mode 打开这个mode的话，semantic会用灰的底色把光标所在函数名高亮显示
;;;; semantic-idle-tag-highlight-mode光标在变量处停留一会，就会把相同的变量全都高亮
;;;; semantic-decoration-on-*-members把private和protected的函数用颜色标识出来
;;;; which-func-mode把光标当前所在的函数名显示在mode-line上

;(semantic-load-enable-semantic-debugging-helpers) ;;;;除enable semantic-load-enable-excessive-code-helpers之外还包含以下三个mode
;;;semantic-highlight-edits-mode emacs会把最近修改过的内容高亮出来
;;;semantic-show-unmatched-syntax-mode这个mode会把semantic解析不了的内容用红色下划线标识出来
;;;semantic-show-parser-state-mode semantic会在modeline上显示出当前解析状态

;;;关闭将函数名显示在Buffer顶的功能，因为容易和tabbar冲突
;(global-semantic-stickyfunc-mode -1)
;;关闭semantic的自动补全功能，因为会很慢，而且和补全插件有点冲突额
;(global-semantic-idle-completions-mode -1)

(when window-system
  (global-semantic-tag-folding-mode 1))
  
(eal-define-keys-commonly
 global-map
 `(("C-x M-j" semantic-complete-jump)))

(defun cedet-semantic-settings ()
  "Settings for `semantic'."
  (eal-define-keys
   `(c-mode-base-map makefile-gmake-mode-map python-mode-map perl-mode-map sh-mode-map)
   `(("C-c C-j" semantic-ia-fast-jump)
     ("C-c j"   semantic-complete-jump-local)
     ("C-c n"   senator-next-tag)
     ("C-c p"   senator-previous-tag)))

  (defun cedet-semantic-settings-4-emaci ()
    "cedet `semantic' settings for `emaci'."  
    (emaci-add-key-definition
     "." 'semantic-ia-fast-jump
     '(memq major-mode dev-modes))
    (emaci-add-key-definition
     "," 'recent-jump-backward
     '(memq major-mode dev-modes)))

  (eval-after-load "emaci"
    `(cedet-semantic-settings-4-emaci))

  (eal-define-keys
   'emaci-mode-map
   `(("." emaci-.)
     ("," emaci-\,)))
  
	;;;关闭将函数名显示在Buffer顶的功能，因为容易和tabbar冲突
	;(global-semantic-stickyfunc-mode -1)
	;;关闭semantic的自动补全功能，因为会很慢，而且和补全插件有点冲突额
	;(global-semantic-idle-completions-mode -1)
	
	;(global-semantic-idle-scheduler-mode -1)
	;(global-semanticdb-minor-mode  -1)
	;(global-semanticdb-load-ebrowse-caches -1)
  
  ;; system include path
  (if (or mswin cygwin)
     (dolist (mode '(c-mode c++-mode))
       (semantic-add-system-include "D:/develop-tools/MinGW/include" mode))))
  
  

(defun semantic-decorate-include-settings ()
  "Settings of `semantic-decorate-include'."
  (eal-define-keys
   'semantic-decoration-on-include-map
   `(("." semantic-decoration-include-visit))))

(defun cedet-semantic-idle-settings ()
  "Settings for `semantic-idle'."
  (defun semantic-idle-tag-highlight-idle-command ()
    "Highlight the tag, and references of the symbol under point.
	Call `semantic-analyze-current-context' to find the refer ence tag.
	Call `semantic-symref-hits-in-region' to identify local references."
    (interactive)
    (semantic-idle-tag-highlight-idle-function))

  (defun semantic-idle-summary-idle-command ()
    "Display a tag summary of the lexical token under the cursor.
	Call `semantic-idle-summary-current-symbol-info' for getting the current tag to display information."
    (interactive)
    (semantic-idle-summary-idle-function))

  (defun semantic-refresh-tags ()
    "Execute `semantic-idle-scheduler-refresh-tags'"
    (interactive)
    (semantic-idle-scheduler-refresh-tags))
  
  (eal-define-keys
   `(c-mode-base-map)
   `(("C-c M-s" semantic-idle-summary-idle-command))))

(defun semantic-decorate-mode-settings ()
  "Settings of `semantic-decorate-mode'."
  (defun semantic-decoration-decorate ()
    "`semantic-decorate-add-decorations' all `semantic-fetch-available-tags'."
    (interactive)
    (semantic-decorate-add-decorations (semantic-fetch-available-tags))))

(eal-define-keys
 `(semantic-symref-results-mode-map)
 `(("1" delete-other-windows)
   ("2" split-window-vertically)
   ("3" split-window-horizontally)
   ("q" delete-current-window)))

(eval-after-load "semantic-decorate-include"
  `(semantic-decorate-include-settings))

(eval-after-load "semantic-decorate-mode"
  `(semantic-decorate-mode-settings))

(eval-after-load "semantic-idle"
  `(cedet-semantic-idle-settings))

(eval-after-load "semantic"
  `(cedet-semantic-settings))

(provide 'cedet-semantic-settings)

;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "开始加载ecb配置(ecb-settings.el)")

(require 'ecb-autoloads)
(custom-set-variables 
	 '(ecb-options-version "2.40"))

(setq stack-trace-on-error nil
	  ecb-tip-of-the-day nil)

(global-set-key [\C-f12] 'ecb-activate)         ;启用ECB
(global-set-key [\C-S-f12] 'ecb-deactivate)     ;退出ECB

(defun ecb ()
  "启动ecb"
  (interactive)
  (ecb-activate)
  (ecb-layout-switch "left9"))

(defun ecb-settings ()	
  "Settings for `ecb'.")

(eval-after-load "ecb"
  `(ecb-settings))

(message  "ecb配置加载成功!")
(provide 'ecb-settings)

;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;

(message  "开始加载字体设置font-config.el")

;; For Windows Ctrl 加上鼠标滚轮操作来设置字体大小
(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease) 

;开始设置字体
;以下四个函数为设置字体
;判断某个字体在系统中是否安装
(defun qiang-font-existsp (font)
  (if (null (x-list-fonts font))
      nil t))
	  
;要按顺序找到一个字体列表( list )中第一个已经安装可用的字体
(defvar font-list '("新宋体" "宋体" "黑体" "Microsoft Yahei"))
(require 'cl) ;; find-if is in common list package
(find-if #'qiang-font-existsp font-list)

;用来产生带上 font size 信息的 font 描述文本
(defun qiang-make-font-string (font-name font-size)
  (if (and (stringp font-size)
           (equal ":" (string (elt font-size 0))))
    (format "%s%s" font-name font-size)
	(format "%s %s" font-name font-size)))

;自动设置字体函数
(defun qiang-set-font (english-fonts
                       english-font-size
                       chinese-fonts
                       &optional chinese-font-size)
  "english-font-size could be set to \":pixelsize=18\" or a integer.
If set/leave chinese-font-size to nil, it will follow english-font-size"
  (require 'cl)
  (let ((en-font (qiang-make-font-string
                  (find-if #'qiang-font-existsp english-fonts)
                  english-font-size))
        (zh-font (font-spec :family (find-if #'qiang-font-existsp chinese-fonts)
                            :size chinese-font-size)))  
;; Set the default English font
;; The following 2 method cannot make the font settig work in new frames.
;; (set-default-font "Consolas:pixelsize=18")
;; (add-to-list 'default-frame-alist '(font . "Consolas:pixelsize=18"))
;; We have to use set-face-attribute 
    (message "Set English Font to %s" en-font)
    (set-face-attribute
     'default nil :font en-font) 
    ;; Set Chinese font 
    ;; Do not use 'unicode charset, it will cause the english font setting invalid
    (message "Set Chinese Font to %s" zh-font)
    (dolist (charset '(kana han symbol cjk-misc bopomofo))
      (set-fontset-font (frame-parameter nil 'font)
                        charset
                        zh-font))))
;设置字体
(qiang-set-font
 '( "新宋体" "Consolas" "Monaco" "DejaVu Sans Mono" "Monospace" "Courier New") ":pixelsize=18"
 '("新宋体" "Microsoft Yahei" "文泉驿等宽微米黑" "黑体" "宋体"))
;设置字体结束

(message "字体加载完毕！")
(provide 'font-config)
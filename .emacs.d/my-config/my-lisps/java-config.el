﻿;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "开始加载java配置(java-config.el)")
;; java配置依赖于cedet，所以请在之前加载cedet配置文件
;(require 'cedet-settings)
;; 当有问题出现显示错误信息，便于调试
(setq debug-on-error t)
;(toggle-debug-on-error t) 

(defun screen-width nil -1)
(define-obsolete-function-alias 'make-local-hook 'ignore "21.1")

; (setq semantic-load-turn-everything-on t)
; (setq global-semantic-highlight-edits-mode 0)
; (setq global-semantic-show-unmatched-syntax-mode 0)
; (setq global-semantic-show-parser-state-mode 0)
; (semantic-load-enable-excessive-code-helpers)
; (semantic-load-enable-semantic-debugging-helpers)


; (global-semantic-stickyfunc-mode -1) ;;关闭将函数名显示在Buffer顶的功能，因为容易和tabbar冲突
; (global-semanticdb-minor-mode 1)
; (global-semantic-idle-scheduler-mode 1)
; (global-semantic-idle-summary-mode 1)
; (global-semantic-decoration-mode 1)
; (global-semantic-highlight-func-mode 1)
; (global-semantic-mru-bookmark-mode -1)
; (global-semantic-idle-completions-mode -1);;;关闭semantic的自动补全功能，因为会很慢，而且和补全插件有点冲突额

;; 不自动加载jde-mode
(setq defer-loading-jde t)
; ;;编辑java文件时加载jde	  
(if defer-loading-jde  
	(progn  
	  (autoload 'jde-mode "jde" "JDE mode." t)
      (setq auto-mode-alist  
			(append  
			 '(("\\.java\\'" . jde-mode))  
         auto-mode-alist)))
   (require 'jde))

;; 用JDK1.5编译如果出现乱码,加上下面一行可以解决
(setq jde-compiler (quote ("javac" "")))

(setq jde-debugger (quote ("JDEbug")))


(setq jde-complete-function (quote jde-complete-menu))

;; 默认的是 Script 在 window下不是找不到文件，就是说ant不可执行 
;; 设置为Ant Server就好了
(setq jde-ant-invocation-method (quote ("Ant Server")))
 
;; Sets the basic indentation for Java source files to two spaces
(defun my-jde-mode-hook ()
  (setq c-basic-offset 4))
(add-hook 'jde-mode-hook 'my-jde-mode-hook)

;;java用ant构建
(setq compile-command "ant -emacs")

(custom-set-variables
   '(jde-jdk-registry (quote (("jdk1.6.0_26" . "D:/develop-tools/jdk1.6.0_26"))))
   '(jde-jdk (quote("jdk1.6.0_26")))
   '(jde-check-version-flag nil)  ;;关闭jde版本检查,否则在cedet1.1版本中会报错,而且java语法不会加亮显示.
   '(jde-build-function (quote (jde-ant-build))) ;; 使用ant 构建项目，默认是make 
   '(jde-ant-args "-emacs") ; compile output in emacs format ,这个不知有没有用，先放这
   '(jde-ant-complete-target t)
   '(jde-enable-abbrev-mode t) ;;自动补全
   '(jde-ant-enable-find t)  ;;如果当前目录没有build.xml会搜索子目录
   '(jde-ant-read-args nil) ;; ant脚本可能要传入一些参数，jde会要求用户输入，这里禁用之
   '(jde-ant-read-buildfile nil);; 不要求用户输入build文件具体的名字，使用默认的build.xml
   '(jde-ant-read-target nil);; 不要求用户输入调用哪个target ,即调用build.xml文件中默认的target
;; '(jde-ant-use-global-classpath t);;使用jde的 global-classpath 
   '(jde-gen-buffer-boilerplate (quote (nil "////////////////////////////////////////////////////////////////////////////////  
     //Copyright (c) 2011-2012 JOY  
     //Author:冯振平  
     //Date：
     //Title:
     ////////////////////////////////////////////////////////////////////////////////")))
  )
  
;;使用ant编译,自动向上查找build.xml
(defun ant-compile ()
  "Traveling up the path, find build.xml file and run compile."
  (interactive)
  (with-temp-buffer
    (while (and (not (file-exists-p "build.xml"))
                (not (equal "/" default-directory)))
      (cd ".."))
    (call-interactively 'compile "ant -emacs")))
	
;;使用jde build java时使用自定义函数ant-compile
'(jde-build-use-make (quote(ant-compile)))
'(jde-build-function (quote(ant-compile)))
'(jde-make-program "ant -emacs")

;;编译完成关闭编译窗口
(setq compilation-finish-functions
      (lambda (buf str)
        (when (and (string= (buffer-name buf) "*compilation*")
                  (not (string-match "exited abnormally" str)))
          (run-at-time 0.5 nil 'delete-windows-on buf)
          )))

(message  "Java配置加载成功!")
(provide 'java-config)

;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "准备加载主题配置(color-theme-config.el)")

(require 'color-theme-autoloads)

(defun color-theme-config ()
  "Settings for `color-theme'."
	(load "color-theme-library")
	(color-theme-dark-blue)
	
	(require 'util)
	(apply-define-key
		color-theme-mode-map
		`(("'"   switch-to-other-buffer)
			("u"   View-scroll-half-page-backward)
			("SPC" scroll-up)
			("1"   delete-other-windows)
			("."   find-symbol-at-point)))
)

(eval-after-load "color-theme-config"
  `(color-theme-config))

(message "主题加载完毕！")
(provide 'color-theme-config)
﻿(message  "开始加载wiki-muse-config配置(wiki-muse-config.el)")
;(setq load-path (cons "~/.emacs.d/my-config/plugins/wiki/muse-3.20/lisp" load-path))
;(setq load-path (cons "~/.emacs.d/my-config/plugins/wiki/muse-3.20/contrib/" load-path))
 
 ; 可以用 Tab 缩进
;(define-key muse-mode-map [tab] 'indent-for-tab-command)
(setq muse-colors-autogen-headings 'outline)

; 默认的 <contents> 标签只总结两层标题
;(setq muse-publish-contents-depth 10)
;(setq max-lisp-eval-depth 800) 

(require 'muse-mode)
(require 'muse-html)
;(require 'muse-latex)
;(require 'muse-texinfo)
(require 'muse-docbook)
(require 'muse-wiki nil t)
(require 'muse-project)

(define-key global-map "\C-c\C-f" 'muse-project-find-file)
(setq muse-html-meta-content-type (concat "text/html; charset=utf-8"))
; 在设置图片位置的时候，以当前编辑文件将要发布到的目录为基准
(setq muse-colors-inline-image-method 'muse-colors-use-publishing-directory)

;;默认html的发布风格
(setq muse-html-style-sheet
      (concat "<link rel=\"stylesheet\" type=\"text/css\""
              " charset=\"utf-8\" media=\"all\""
              " href=\"D:/develop-tools/emacs/wiki/muse/style.css\">"))

; 定制自己的发布风格(my-html)
(muse-derive-style "my-html" "html"
                   :header "~/wiki/muse/html-header.html"
                   :footer "~/wiki/muse/html-footer.html" )
;				   :style-sheet 'my-different-style-sheet)  写进头文件了

(setq muse-project-alist
      '(("Home" ("~/wiki/muse/" :default "index")
			(:base "my-html" :path "~/wiki/muse/publish")
			;(:base "pdf" :path "~/wiki/muse/publish/pdf")
			)
		("python" ("~/wiki/muse/python" :default "index")
			(:base "my-html" :path "~/wiki/muse/publish/python")
			;(:base "pdf" :path "~/wiki/muse/publish/python/pdf")
			)
		("emacs" ("~/wiki/muse/emacs" :default "index")
			(:base "my-html" :path "~/wiki/muse/publish/emacs")
			;(:base "pdf" :path "~/wiki/muse/publish/emacs/pdf")
			)
		("lisp" ("~/wiki/muse/lisp" :default "index")
			(:base "my-html" :path "~/wiki/muse/publish/lisp")
			;(:base "pdf" :path "~/wiki/muse/publish/lisp/pdf")
			)
		("project-manage" ("~/wiki/muse/project-manage" :default "index")
		    (:base "my-html" :path "~/wiki/muse/publish/project-manage")
			;(:base "pdf" :path "~/wiki/muse/publish/project-manage/pdf")
			)
		 ))
		 
;;设置批量发布
(defun muse-project-batch-publish ()
  "Publish Muse files in batch mode.
	发布所有项目 emacs -q -batch -l muse-init.el -f muse-project-batch-publish --force --all
	发布一个项目 muse-project-batch-publish --force python
  "
  (let ((muse-batch-publishing-p t) force)
    (if (string= "--force" (or (car command-line-args-left) ""))
        (setq force t
              command-line-args-left (cdr command-line-args-left)))
    (if (string= "--all" (or (car command-line-args-left) ""))
        (setq command-line-args-left (nconc (cdr command-line-args-left)
                                            (mapcar 'car muse-project-alist))))
    (if command-line-args-left
        (dolist (project (delete-dups command-line-args-left))
          (message "Publishing project %s ..." project)
          (muse-project-publish project force))
      (message "No projects specified."))))
	  
;;文档更新日期
(setq muse-completing-read-function 'ido-completing-read
      muse-publish-date-format "%Y 年 %m 月 %d 日")

(message  "wiki-muse配置加载成功!")
(provide 'wiki-muse-config)
;;end of file

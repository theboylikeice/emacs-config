﻿;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;

(message  "准备加载基本配置base-config.el")

;;设置窗口位置为屏库左上角(0,0)
(set-frame-position (selected-frame) 0 0)
;;设置宽和高
(set-frame-width (selected-frame) 110)
(set-frame-height (selected-frame) 33)

;启动最大化
(require 'maxframe)
;(setq mf-max-width 1018)
;(setq mf-max-height 730)
(add-hook 'window-setup-hook 'maximize-frame t)

 ;;设置有用的个人信息.这在许多地方有用。
(setq user-full-name "无很")
(setq user-mail-address "wuhen86@gmailmail.com.cn")
;;在标题栏显示buffer的名字。
;(setq frame-title-format "emacs@%b")
;;设置标题栏显示文件路径
(defun frame-title-string ()
  "Return the file name of current buffer, using ~ if under home directory"
  (let
      ((fname (or
	       (buffer-file-name (current-buffer))
	       (buffer-name)))
       (max-len 100))
    (when (string-match (getenv "HOME") fname)
      (setq fname (replace-match "~" t t fname)))
    (if (> (length fname) max-len)
	(setq fname
	      (concat "..."
		      (substring fname (- (length fname) max-len)))))
    fname))
(setq frame-title-format '("无很 - Emacs@"(:eval (frame-title-string))))

;;取消tab
(setq indent-tabs-mode nil)
(setq default-tab-width 4)

;;取消警告声音
(setq visible-bell t)
;;支持 emacs 和外部程序的粘贴
(setq x-select-enable-clipboard t)
;;取消滚动条
(scroll-bar-mode nil)
;;显示列号
(setq column-number-mode t)
(global-linum-mode t) 
;;高亮显示选择区域
(setq-default transient-mark-mod t)
(transient-mark-mode t)
;;括号匹配显示
(show-paren-mode t)
;;隐藏工具栏
(tool-bar-mode -1)
;;隐藏菜单栏
;;(menu-bar-mode -1)
;;语法高亮
(global-font-lock-mode t)
;;关闭启动画面
(setq inhibit-startup-message t)
;;把 fill-column 设为 78. 这样的文字更好读。
(setq default-fill-column 78)
;不要生成临时文件
(setq-default make-backup-files nil)
;;不产生备份
(setq backup-inhibied t)
;;不生成名为#filename#的临时文件
(setq auto-save-default nil)
;;退出时询问
(setq kill-emacs-query-functions
      (lambda ()(y-or-n-p "Do you really want to quit? ")))
;; 设置时间戳，标识出最后一次保存文件的时间。
(setq time-stamp-active t)
(setq time-stamp-warn-inactive t)
(setq time-stamp-format "%:y-%02m-%02d %3a %02H:%02M:%02S JOY")
;;设置M-g为goto-line
(global-set-key (kbd "M-g") 'goto-line)
;显示时间
(display-time)
;不要总是没完没了的问yes or no, 为什么不能用 y/n 
(fset 'yes-or-no-p 'y-or-n-p)


(defun kill-shell  ()
   (set-process-sentinel (get-buffer-process (current-buffer))
                            #'kill-shell-buffer-on-exit))
(defun kill-shell-buffer-on-exit (process state)
  (kill-buffer (current-buffer)))
 ;;退出shell时删除shell buffer                                   
(add-hook 'shell-mode-hook 'kill-shell)

;按照windowz用户的习惯使用 `C-x C-c C-v'
;(setq cua-mode t)

;按照windows用户的习惯,使用 'Ctrl Alt Shift + 方向键移动和选择文本'
;(setq pc-selection-mode t)

;解决emacs shell 乱码 
(setq ansi-color-for-comint-mode t) 
(customize-group 'ansi-colors) 
(kill-this-buffer);关闭customize窗口

;; 用M-x执行某个命令的时候，在输入的同时给出可选的命令名提示
(icomplete-mode 1)
(setq-default kill-whole-line t);;C-k 删除该行

;;编译成功后自动关闭*compilation* 函数
(defun kill-buffer-when-compile-success (process)
"Close current buffer when `shell-command' exit."
	(set-process-sentinel process
		(lambda (proc change)
			(when (string-match "finished" change)
				(delete-windows-on (process-buffer proc))))))

;; 编译成功后自动关闭*compilation* buffer
(add-hook 'compilation-start-hook 'kill-buffer-when-compile-success)

;; 先格式化再补全
(setq tab-always-indent 'complete)

;;;;切换到scratch buffer
;;(switch-to-buffer "*scratch*") 
;;;;删除其他 buffer
;;(delete-other-windows)
;;;;初始化 scratch buffer 为空(不显示里面的注释)
(setq initial-scratch-message "")

(message  "基本配置加载完毕！")

(provide 'base-config)

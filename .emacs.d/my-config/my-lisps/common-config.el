﻿;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "开始加载常用配置(common-config.el)")

;;;;加载历史记录
;(require 'history)

;;;;使用unicad插件，自动识别文件编码
(require 'unicad)

;;;;标签分组
(require 'tabbar)
(tabbar-mode)
(define-prefix-command 'lwindow-map)
(global-set-key (kbd "<M-up>") 'tabbar-backward-group)
(global-set-key (kbd "<M-down>") 'tabbar-forward-group)
(global-set-key (kbd "<M-left>") 'tabbar-backward)
(global-set-key (kbd "<M-right>") 'tabbar-forward)
;;设置tabbar结束

;;退出的时候删掉server文件(注意:server文件夹需要用户权限)
(add-hook 'kill-emacs-hook'
	(lambda()
		(if (file-exists-p "~/.emacs.d/server/server")
			(delete-file "~/.emacs.d/server/server"))))
;;启动GNU Server
(server-start)
(server-mode 1)

;注释和反注释代码的操作只使用一个 Alt-; 键就全部搞定了
(defun qiang-comment-dwim-line (&optional arg)
  "Replacement for the comment-dwim command.
If no region is selected and current line is not blank and we are not at the end of the line,
then comment current line.
Replaces default behaviour of comment-dwim, when it inserts comment at the end of the line."
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    (comment-dwim arg)))
(global-set-key "\M-;" 'qiang-comment-dwim-line)

;Ctrl-Alt-\ 格式化
(dolist (command '(yank yank-pop))
  (eval
   `(defadvice ,command (after indent-region activate)
      (and (not current-prefix-arg)
           (member major-mode
                   '(emacs-lisp-mode
                     lisp-mode
                     clojure-mode
                     scheme-mode
                     haskell-mode
                     ruby-mode
                     rspec-mode
                     python-mode
					 java-mode
                     c-mode
                     c++-mode
                     objc-mode
                     latex-mode
                     js-mode
                     plain-tex-mode))
           (let ((mark-even-if-inactive transient-mark-mode))
             (indent-region (region-beginning) (region-end) nil))))))

(message  "常用配置加载完毕！")
(provide 'common-config)

;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "开始加载doxymacs配置(doxymacs-settings.el)")
(setq debug-on-error t)
;(add-to-list 'load-path (expand-file-name "~/.emacs.d/my-config/plugins/doxymacs"))

;;;;doxymacs需要使用cedet，所在先加载cedet-config
(require 'cedet-settings)

(require 'doxymacs)

(defun my-doxymacs-font-lock-hook ()
  (if (or (eq major-mode 'c-mode) (eq major-mode 'c++-mode) (eq major-mode 'lisp-mode))
      (doxymacs-font-lock)))
	  
;;(add-hook 'font-lock-mode-hook 'my-doxymacs-font-lock-hook)

(doxymacs-mode t);doxymacs-mode常true
;(global-set-key [(f6)] 'doxymacs-mode);doxymacs-mode快捷键[F6]
(add-hook 'c-mode-common-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'c++-mode-common-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'java-mode-common-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'emacs-lisp-mode-hook 'doxymacs-mode) ;; 启动doxymacs-mode 
(add-hook 'lisp-mode-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'clojure-mode-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'scheme-mode-hook  'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'ruby-mode-hook  'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'python-mode-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'java-mode-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'c-mode-hook 'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'c++-mode-hook  'doxymacs-mode) ;; 启动doxymacs-mode
(add-hook 'js-mode-hook 'doxymacs-mode) ;; 启动doxymacs-mode

;;;;文件默认注释
(defconst doxymacs-JavaDoc-file-comment-template
'(
   "/********************************************************************" > n   
   "** " (doxymacs-doxygen-command-char) "file    "
   (if (buffer-file-name)
       (file-name-nondirectory (buffer-file-name))
     "") > n
   "** " (doxymacs-doxygen-command-char) "author  " (user-full-name)   > n  
   "** " (doxymacs-doxygen-command-char)"date    " (format-time-string  "%Y-%02m-%02d %02H:%02M:%02S" (current-time)) > n  
   "** " (doxymacs-doxygen-command-char)"brief   " > n  
   "**" "    "> n
   "** " "    "> n   
   "**" (doxymacs-doxygen-command-char) "version 0.1" > n
   " " " Copyright (c) 2012, JOY工作室"> n  
   "********************************************************************/"> n)
 "Default JavaDoc-style template for file documentation.")


 
 

;;;;定义c++文件注释 
(defconst doxymacs-C++-file-comment-template
 '(
   "/******************************************************************************" > n
   "*" > n
   "* " "FILE NAME   :"
   (if (buffer-file-name)
	   (file-name-nondirectory (buffer-file-name))
	 "") > n
   "*" > n 
   "*" "Author      :无很(wuhen86@gmail.com)"> n
   "*" > n
   "*" "Version     :v1.0"> n
   "*" > n
   "*" "Date        :"
   (format-time-string  "%Y-%02m-%02d %02H:%02M:%02S" (current-time)) > n
   "*" > n
   "*" " DESCRIPTION :"> n
   "*" > n
   "*" "	"> n
   "*" > n
   "*" " HISTORY	 :"> n
   "*" > n
   "*" "	See Log at end of file"> n
   "*" > n
   "*" " Copyright (c) 2012, JOY工作室."> n
   "*" "******************************************************************************/"> n)
 "Default C++-style template for file documentation.")
 

 
 
 ;;;;定义python文件注释
(defconst doxymacs-Python-file-comment-template
 '(
   """******************************************************************************" > n
   "*" > n
   "* " "FILE NAME   :"
   (if (buffer-file-name)
	   (file-name-nondirectory (buffer-file-name))
	 "") > n
   "*" > n
   "*" " DESCRIPTION :"> n
   "*" > n
   "*" "	"> n
   "*" > n
   "*" " HISTORY	 :"> n
   "*" > n
   "*" "	See Log at end of file"> n
   "*" > n
   "*" " Copyright (c) 2012, JOY工作室."> n
   "*" "******************************************************************************/"""> n)
 "Default Python-style template for file documentation.")
 
;;;;定义C-cdp插入Python文件注释
(define-key doxymacs-mode-map "\C-cdp"
  'doxymacs-insert-file-comment)

(defun doxymacs-insert-python-file-comment ()
  "Inserts Doxygen documentation for the current file at current point."
  (interactive "*")
  (doxymacs-call-template "Python-file-comment"))

;;;;定义Lisp注释
(defconst doxymacs-Lisp-file-comment-template
 '(
   "#| " > n
    "@File Name   :" (if (buffer-file-name)
						(file-name-nondirectory (buffer-file-name))
					  ) > n
   "@Author      :无很(wuhen86@gmail.com)"> n
   "@Version     :v1.0"> n
   "@Date        :"   (format-time-string  "%Y-%02m-%02d %02H:%02M:%02S" (current-time)) > n
   "@Description :"> n
   "@History	    :"> n
   "@Copyright (c) 2012, JOY工作室."> n
   "|#"> n)
 "Default Lisp-style template for file documentation.")
 
;;;; 定义C-cdl插入Lisp文件注释
(define-key doxymacs-mode-map "\C-cdl"
  'doxymacs-insert-lisp-file-comment)
  
(defun doxymacs-insert-lisp-file-comment ()
  "Inserts Doxygen documentation for the current file at current point."
  (interactive "*")
  (doxymacs-call-template "Lisp-file-comment"))
 

(message  "doxymacs配置加载成功!")
 
(provide 'doxymacs-settings)

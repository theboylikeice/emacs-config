;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "开始加载中文配置(chinese-menu.el)")

;(load "menu-bar")
;(load "facemenu")
;;;;;(load "bookmark")
;
;(load "mule-cmds")
;(load "chinese")
;(load "eudc")
;(load "ediff-hook")
;(load "ispell")
;; --------------------

(require 'menu-bar)
(require 'facemenu)
(require 'mule-cmds)
(require 'chinese)
(require 'eudc)
(require 'ediff-hook)
(require 'ispell)
(load "menu-bar")
 
;
;; codings for Emacs Windows/zh_CN
;(set-language-environment 'Chinese-GB)




;(set-language-environment 'UTF-8)
;(set-keyboard-coding-system 'euc-cn)
;(set-clipboard-coding-system 'euc-cn)
;(set-terminal-coding-system 'euc-cn)
;(set-buffer-file-coding-system 'euc-cn)
;(set-selection-coding-system 'euc-cn)
;(modify-coding-system-alist 'process "*" 'euc-cn)
;(setq default-process-coding-system '(euc-cn . euc-cn))
;(setq-default pathname-coding-system 'euc-cn)
;(setq file-name-coding-system 'euc-cn)
;(prefer-coding-system 'cp950)
;(prefer-coding-system 'gb2312)
;(prefer-coding-system 'cp936)
;(prefer-coding-system 'gb18030) 
;(prefer-coding-system 'utf-16)
;(prefer-coding-system 'utf-8)
(message "中文配置加载成功！")
 
(provide 'chinese-menu)

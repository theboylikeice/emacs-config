﻿;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "开始加载php配置(php-config.el)")

;;打开php模式
(require 'php-mode)
(add-hook 'php-mode-user-hook 'turn-on-font-lock)

(message  "PHP配置加载成功!")
(provide 'php-config)

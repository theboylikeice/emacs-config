﻿;;;; __*__ coding:utf-8 __*__
;;;; 作者: 无很<wuhen86@gmail.com>
;;;; URL:  http://wuhen.cnblogs.com
;;;; Version:1.1
;;;; 日期: 2012
;;;; 说明: 玩emacs已经有些时间了，一些主要的配置大部分是从网上摘抄来的，
;;;;      在此感谢为emacs默默奉献的emacser。
;;;;  
;;;;
(message  "开始加载common lisp配置(common-lisp-config.el)")
;(add-to-list 'load-path "~/.emacs.d/my-config/lisps/lisp/slime-current")
(add-to-list 'load-path "c:/common-tools/lisp/sbcl")
(add-to-list 'load-path "c:/common-tools/lisp/ccl")

;(setq slime-net-coding-system 'utf-8-unix)
;;(setq inferior-lisp-program "sbcl")
;;;;添加多个lisp实现;  按 M +  -，M + x slime lisp实现名启动一个lisp
;(setq slime-lisp-implementations
;      '((sbcl ("c:/common-tools/lisp/sbcl/sbcl.exe" ) :coding-system utf-8-unix)	    
;		(ccl ("c:/common-tools/lisp/ccl/wx86cl.exe") :coding-system utf-8-unix)
;		))

(require 'slime-autoloads)

;;(slime-setup '(slime-fancy slime-tramp slime-asdf))

;(eval-after-load "slime"
;  '(progn
	 (slime-setup '(slime-fancy slime-asdf slime-banner))
	 ;(setq slime-complete-symbol*-fancy t)
	 ;(setq slime-complete-symbol-function 'slime-fuzzy-complete-symbol)
	 ;(load-file "c:/Users/Administrator/quicklisp/setup.lisp" );;加载quicklisp
;   ))

;;自动补全
;(defun lisp-indent-or-complete (&optional arg)
;  (interactive "p")
;  (if (or (looking-back "^\\s-*") (bolp))
;      (call-interactively 'lisp-indent-line)
;      (call-interactively 'slime-indent-and-complete-symbol)))
;(eval-after-load "lisp-mode"
;  '(progn
;     (define-key lisp-mode-map (kbd "TAB") 'lisp-indent-or-complete)))

;(require 'slime)
;(slime-setup '(slime-fancy))


;;; 打开Lisp源文件时启动SLIME
(add-hook 'slime-mode-hook	  
	(lambda ()	    
		(unless (slime-connected-p)
			(save-excursion (slime)))))
 
;;; 绑定C-c e组合键为利用macroexpand-1函数展开当前宏表达式
(global-set-key "\C-ce" 'slime-macroexpand-1)



(eval-after-load "slime"
	`(progn
	(slime-setup '(slime-repl slime-fancy slime-asdf slime-banner))
	(custom-set-variables
		'(inhibit-splash-screen t)
		'(slime-complete-symbol*-fancy t)
		'(slime-complete-symbol-function 'slime-fuzzy-complete-symbol)
		'(slime-net-coding-system 'utf-8-unix)
		'(slime-startup-animation nil)
		'(common-lisp-hyperspec-root "~/.emacs.d/my-config/lisps/lisp/HyperSpec/") ;;; 设置HyperSpec文档在本机上的路径
		'(browse-url-browser-function  '(("~/.emacs.d/my-config/lisps/lisp/HyperSpec/" . w3m-browse-url)	
										 ("." . browse-url-browser-function))) ;;; 在Emacs-w3m中打开HyperSpec文档
		'(slime-lisp-implementations '((sbcl ("c:/common-tools/lisp/sbcl/sbcl.exe" ) :coding-system utf-8-unix)	    
									   (ccl ("c:/common-tools/lisp/ccl/wx86cl.exe") :coding-system utf-8-unix))))))

(message  "Common Lisp配置加载完毕!")
(provide 'common-lisp-config)

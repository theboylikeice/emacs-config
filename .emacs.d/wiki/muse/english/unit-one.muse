Unit 1 
Where there is a will,there is a way!!   有志者,事竟成!

 1 One
abandon     放弃,抛弃
ability     能力,才能,才智
able        有能力的,能力出众的,有...能力
aboard      在(船,飞机,车)上,上(船,飞机,车)
abroad      在(到)国外,在流传

absent      缺席(的时间);缺乏;不(存)在
absence     (from)缺席的;不存在的;心不在焉的
absolute    绝对的;完全的;不受限制的
absort      吸收;吸引...的注意;使全神贯注
abstract    抽象(派)的;
            摘要;梗概;抽象派艺术作用;
            提取;抽取;做...的摘要
abundant    丰富的;大量的;充足的;
abuse       滥用;虐待;辱骂
academic    学院的;学术的;纯理论的;
            大学教师
acadimy     研究所,学会;专门学校,学院
accelerate  (使)加快,增速
accent      口音,腔调,重音(符号)
            重读
accept      vt 接受;承认;想念
acceptable  可接受的;值得接受的;合意的
acceptance  n 接受;赞同;承认;容忍
access      通道;入口;进入,(to)接近(的机会)
            存取(计算机文件)
 2 Two
accident    意外遭遇;事故;意外(因素)
accidental  意外的,偶然(发生)的
accommodation 住处,膳宿
accompany   陪伴,陪同;伴随;伴奏
accomplish  vt 完成,实现
accord      n 谅解;协议
            vi/n(with) (相)符合,(相)一致的;
            vt 赠与,给予
accordance  n 一致,和谐,符合
according to 根据,按照
accordingly ad 照着,相应地;因此,于是
account     n 叙述;报告;帐户;解释
            vi (for)说明...的原因;(在数量,比例方面)占
accountant  会计人员,会计师
accumulate  vt 堆积;积累;积聚; vi 累积;聚积
accuracy    准确(性),精确(性)
accurate    正确无误的;准确的,精确的
accuse      指控,控告,指责
accustomed  (to)习惯于...的;通常的,惯常的
achieve     vt 完成;实现 vi 成功
achievement 成就,成绩;达到;完成
acid        酸(性物质) a.酸(味)的;尖刻的;刻薄的
acknowledge vt.承认;告知收到,确认;对...表示感谢

 3 Three 
acquaintance 相识的人,熟人,认识;了解
acquire     取得,学得,学到,获得
acquisition n.取得,获得
acre        英亩
action      行动;行为;作用;情节
active      活跃的;不动的;起作用的
activily    n. 行动;活动;活跃
actor       男演员
actress     女演员
actual      实际的;事实上的;真实的
acute       严重的,激烈的;敏锐的;急性的;尖的
adapt       (使)适应,(使)适合,修改,改编
  The book has bean adapted to the needs of children.
  You will soon adapt yourself to the new environment.
  Young animals adapt quickly to a new environent.

addition    n. 加(法);附加物
  in addition 另外,加上,还有
  in addition to 除...外又,加于...上
    In addition to thick fog,there was a heavy swell. 除浓雾之外,又有滚滚大浪.

additional  添加的,额外的,另外的
address     n. 地址;演说;讲话
            vt.写地址;致词;称呼;对付;处理
adequate    (for)充足的,足够的;(to)适当的;胜任的
  He is adequate to the job.
  I want a salary adequate to support my family.
  We had adequate food for a week's journey(旅游)

adjective   形容词
adjust      校正;调节,改变...以适应   (to)适应
  You must adjust yourself to new conditions(条件,环境,情形)

adiministrator 经营;管理;政府;实行;执行
admire      钦配;赞赏;称赞,夸奖  despise 鄙视,看不起
  I admire him for his courage(勇气,精神)

 4 Four 

